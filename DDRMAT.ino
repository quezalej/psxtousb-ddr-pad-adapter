//https://github.com/NicoHood/HID
#include <HID-Project.h>

//https://github.com/SukkoPera/PsxNewLib
#include <DigitalIO.h>
#include <PsxControllerBitBang.h>

//pinout
const byte PIN_PS2_ATT = 20;
const byte PIN_PS2_CMD = 5;
const byte PIN_PS2_DAT = 3;
const byte PIN_PS2_CLK = 18;	

PsxControllerBitBang<PIN_PS2_ATT, PIN_PS2_CMD, PIN_PS2_DAT, PIN_PS2_CLK> psx;

unsigned int data;

void setup() {
  Gamepad.begin();
// serial lines can be uncommented for debug output
//  Serial.begin(9600);
}

void loop(){
  psx.begin ();
  psx.read ();

  data = psx.getButtonWord();
//  Serial.println (data);

  //Cruceta NO FUNCIONA, TENGO QUE ESCRIBIR UNA VARIABLE PARA MANDAR SOLO 1 COMANDO Y RESETEAR CON 1 ELSE POR EJE
 // if(data&PSB_PAD_UP)   Gamepad.yAxis(-32767); 
 // if(data&PSB_PAD_DOWN) Gamepad.yAxis(32767);
 // if(data&PSB_PAD_LEFT) Gamepad.xAxis(-32767); 
 // if(data&PSB_PAD_RIGHT)Gamepad.xAxis(32767);  else Gamepad.xAxis(0);
  

//DDR PAD BUTTONS 
  if(data&PSB_PAD_UP)   Gamepad.press(11); else Gamepad.release(11);
  if(data&PSB_PAD_DOWN)   Gamepad.press(13); else Gamepad.release(13);
  if(data&PSB_PAD_LEFT)   Gamepad.press(14); else Gamepad.release(14);
  if(data&PSB_PAD_RIGHT)  Gamepad.press(12); else Gamepad.release(12);
  
//FACEBUTTONS
  if(data&PSB_CROSS)    Gamepad.press(1); else Gamepad.release(1);
  if(data&PSB_SQUARE)   Gamepad.press(3); else Gamepad.release(3);
  if(data&PSB_TRIANGLE) Gamepad.press(4); else Gamepad.release(4);
  if(data&PSB_CIRCLE)   Gamepad.press(2); else Gamepad.release(2);
  
  if(data&PSB_R1)       Gamepad.press(7); else Gamepad.release(7);
  if(data&PSB_R2)       Gamepad.press(8); else Gamepad.release(8);
  if(data&PSB_L1)       Gamepad.press(5); else Gamepad.release(5);
  if(data&PSB_L2)       Gamepad.press(6); else Gamepad.release(6);

  if(data&PSB_START)    Gamepad.press(9); else Gamepad.release(9);
  if(data&PSB_SELECT)   Gamepad.press(10); else Gamepad.release(10);
  
  

  Gamepad.write(); //update current gamepad status
  
  //update delay in ms
  delay (3);
}

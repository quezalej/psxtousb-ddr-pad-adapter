# Dance Dance Revolution PSX pad to USB adapter

## Background
Since most of the early DDR games were released on Playstation consoles, it is most common to see DDR pads made for the PS1 and PS2. It is notoriously hard to get these pads working on a computer with the available PSX to USB adapters, most of them will not recognize the DDR pad as a controller. Some may recognize it as such, but since the directional pad is mapped to a POV-Hat in Windows, it will be unable to perform "jumps", as the input will not accept opposite directions simultaneously (Up+Down, Left+Right).
Thanks to the PsxNewLib Arduino library by SukkoPera, it is possible to interface an Arduino board with a Playstation controller. This allows mapping the inputs in any way needed by configuring the sketch file.
Some Arduino boards have native USB Human Interface Device support, and require only including a library (HID-Project.h by NicoHood was used in this project) for it to be recognized as an input device by Windows without additional drivers (plug and play).

## Construction and functionality
A native HID compatible Arduino board was used to avoid the extra steps that are required to use the more common Arduino UNO which lacks this feature. In this project an Arduino Pro Micro runs the program, while the female connector to plug the controller was scavenged from the cheapest PSX to USB controller adapter I could find. *Note: the pad did not work at all with the USB adapter but regular controllers were recognized as Playstation 3 controllers by windows and they seemed to work.*

- The PSX to USB adapter from which I took the connector:

![Cheapest PSX to usb Adapter](IMAGES/aliexpressUSB.jpg)

- Testing the wiring

![Testing](IMAGES/labels.jpg)

After taking out the connector, I tested the functionality on a breadboard. I was able to put the entire Arduino Pro Micro board inside the PSX port casing once finished. This makes for a very portable and compact adapter, that only requires a Micro USB cable for connecting to a Windows computer.

- The finished adapter

![Finished](IMAGES/adapterfinA.jpg)
![Finished](IMAGES/adapterfinB.jpg)

The provided sketch uses the following pin configuration:
- Pin 20 = ATT (attention), connects to pin 6 (yellow cable) on the controller.
- Pin 5 = CMD (command), connects to pin 2 (orange cable) on the controller.
- Pin 3 = DAT (data), connects to pin 7 (beown cable) on the controller.
- Pin 18 = CLK (clock), connects to pin 1 (blue cable) on the controller.

## Sketch information
The Arduino [program](DDRMAT.ino) is tasked with interfacing with the controller by using SukkoPera's PsxNewLib, and forwarding the inputs to a Windows computer as a regular gamepad device via the HID-Project library. Most of the code was adapted from this tutorial, which provides information on making a similar adapter, but for a Beatmania IIDX controller and connecting to a mobile device: https://de0.hatenablog.com/entry/2020/09/04/203134 (website in Japanese only).
All the controller mappings are defined in the code, so they can be customized as needed.
No configuration is available on the adapter itself. It will behave in a consistent manner as soon as it's plugged in and a controller is connected to it.